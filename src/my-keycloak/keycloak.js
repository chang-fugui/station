import Keycloak from 'keycloak-js'
import initdata from './keycloak.json'
import paths from './paths'

// eslint-disable-next-line no-unused-vars
class KeycloakClient {
  constructor() {
    this.initialized = false
    this.keycloak = new Keycloak(initdata)
    this.accessConfig = paths
  }
  init = (callback) => {
    this.keycloak.init({ onLoad: 'login-required' })
      .success(authenticated => {
        if (authenticated) {
          this.initialized = true
          console.log(authenticated)
          console.log(this.keycloak.tokenParsed)
          // callback(this.keycloak)
        }
      }).error(info => {
        console.log('报错' + info)
      })
  }
  isAccessable = (userRole, url) => {
    if (!this.accessConfig) {
      return false
    }
    let requiredUrls = this.accessConfig[userRole]
    if (!requiredUrls) {
      return false
    }
    console.log(requiredUrls)
  }
}

const keycloakClient = new KeycloakClient()
export default keycloakClient
