import request from '@/utils/request'

// 登录方法
export function login(username, password, code, uuid) {
  // const data = {
  //   username,
  //   password,
  //   code,
  //   uuid
  // }
  // eslint-disable-next-line no-unused-vars
  const jsonObj = JSON.parse('{"msg":"操作成功","code":200,"token":"eyJhbGciOiJIUzUxMiJ9.eyJsb2dpbl91c2VyX2tleSI6IjgwYjg0ZGYwLTlkNDUtNDNiYy1iNmZkLTkwYjhmYjNmNTVlZiJ9.QxzKYomC8n3tRKhGJaHbRKxBLh7LHir7koCwNHn3njPfxJB1tjxOZKsVL_a_boRbFvB-uPimmyYgpfy7zn1MgQ"}')
  return Promise.resolve(jsonObj)
  // return request({
  //   url: '/login',
  //   method: 'post',
  //   params: data
  // })
}

// 获取用户详细信息
export function getInfo() {
  const jsonObj = JSON.parse('{"msg":"操作成功","code":200,"permissions":["*:*:*"],"roles":["admin"],"user":{"searchValue":null,"createBy":"admin","createTime":"2018-03-16 03:33:00","updateBy":null,"updateTime":null,"remark":"管理员","dataScope":null,"params":{},"userId":1,"deptId":103,"userName":"admin","nickName":"若依","email":"ry@163.com","phonenumber":"15888888888","sex":"1","avatar":"","password":"$2a$10$7JB720yubVSZvUI0rEqK/.VqGOZTH.ulu33dHOiBE8ByOhJIrdAu2","salt":null,"status":"0","delFlag":"0","loginIp":"127.0.0.1","loginDate":"2018-03-16T03:33:00.000+0000","dept":{"searchValue":null,"createBy":null,"createTime":null,"updateBy":null,"updateTime":null,"remark":null,"dataScope":null,"params":{},"deptId":103,"parentId":101,"ancestors":null,"deptName":"研发部门","orderNum":"1","leader":"若依","phone":null,"email":null,"status":"0","delFlag":null,"parentName":null,"children":[]},"roles":[{"searchValue":null,"createBy":null,"createTime":null,"updateBy":null,"updateTime":null,"remark":null,"dataScope":"1","params":{},"roleId":1,"roleName":"管理员","roleKey":"admin","roleSort":"1","status":"0","delFlag":null,"flag":false,"menuIds":null,"deptIds":null,"admin":true}],"roleIds":null,"postIds":null,"admin":true}}')
  return Promise.resolve(jsonObj)
  // return request({
  //   url: '/getInfo',
  //   method: 'get'
  // })
}

// 退出方法
export function logout() {
  const jsonObj = JSON.parse('{"msg":"退出成功","code":200}')
  return Promise.resolve(jsonObj)
  // return request({
  //   url: '/logout',
  //   method: 'post'
  // })
}

// 获取验证码
export function getCodeImg() {
  return request({
    url: '/captchaImage',
    method: 'get'
  })
}
